package semanaactual.maykelalvarado.facci.proyecto;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class SubirProducto extends AppCompatActivity {

    Spinner spinnerTipoFlores, spinnerTipoAdorno, spinnerNumeroFlores;
    ImageView ivGuardar, ivBuscar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_subir_producto );

        ivBuscar = (ImageView)findViewById( R.id.ivBuscar );
        ivGuardar = (ImageView)findViewById( R.id.ivGuardar );


        ivBuscar.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent (SubirProducto.this, MisProductos.class);
                startActivity( intent );
            }
        } );


        ivGuardar.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText( getApplicationContext(), "GUARDADO CON EXITO", Toast.LENGTH_LONG ).show();
            }
        } );


        spinnerTipoFlores = (Spinner)findViewById( R.id.spinnerTipoFlores );

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.lista, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.preference_category);
        spinnerTipoFlores.setAdapter(adapter);


        spinnerTipoAdorno = (Spinner)findViewById( R.id.spinnerTipoArreglo);

        ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(this, R.array.listaAdorno, android.R.layout.simple_spinner_item);
        adapter1.setDropDownViewResource(android.R.layout.preference_category);
        spinnerTipoAdorno.setAdapter(adapter1);


        spinnerNumeroFlores = (Spinner)findViewById( R.id.spinnerNumeroFlores);

        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this, R.array.numero, android.R.layout.simple_spinner_item);
        adapter2.setDropDownViewResource(android.R.layout.preference_category);
        spinnerNumeroFlores.setAdapter(adapter2);


    }
}
