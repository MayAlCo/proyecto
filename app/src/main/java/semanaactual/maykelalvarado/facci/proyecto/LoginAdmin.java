package semanaactual.maykelalvarado.facci.proyecto;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class LoginAdmin extends AppCompatActivity {

    Button btnIniciarSeccionAdmin, btnRegistrarseAdmin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_login_admin );

        btnIniciarSeccionAdmin = (Button)findViewById( R.id.btnIniciarSecionAdmin );
        btnRegistrarseAdmin =(Button)findViewById( R.id.btnRegistrarseAdmin);

       btnRegistrarseAdmin.setOnClickListener( new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               Intent intent = new Intent( LoginAdmin.this, RegistroAdmin.class );
               startActivity( intent );
           }
       } );


       btnIniciarSeccionAdmin.setOnClickListener( new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               Intent intent = new Intent( LoginAdmin.this, InicioAdmin.class );
               startActivity( intent );
           }
       } );


    }
}
