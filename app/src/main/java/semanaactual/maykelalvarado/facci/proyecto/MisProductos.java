package semanaactual.maykelalvarado.facci.proyecto;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import ahmed.easyslider.EasySlider;
import ahmed.easyslider.SliderItem;

public class MisProductos extends AppCompatActivity {

    EasySlider easySlider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_mis_productos );

        easySlider = findViewById( R.id.slider );

        List<SliderItem> easySliders = new ArrayList<>(  );

        easySliders.add( new SliderItem( "FLORES MATRIMONIO", R.drawable.fondo  ) );
        easySliders.add( new SliderItem( "FLORES BAUTIZO", R.drawable.fondo2  ) );
        easySliders.add( new SliderItem( "FLORES QUINCEAÑERA", R.drawable.fondo3  ) );
        easySliders.add( new SliderItem( "FLORES FIESTA", R.drawable.fondo4  ) );
        easySliders.add( new SliderItem( "FLORES COMUNION", R.drawable.fondo5  ) );

        easySlider.setPages( easySliders );
    }
}
