package semanaactual.maykelalvarado.facci.proyecto;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class InicioAdmin extends AppCompatActivity {

    ImageView ivMisProductos, ivSubirProducto, ivPedidoAdmin, ivMiGanancia, ivSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_inicio_admin );

        ivMisProductos= (ImageView)findViewById( R.id.ivMisProductos );
        ivSubirProducto= (ImageView)findViewById( R.id.ivSubirProducto );
        ivPedidoAdmin = (ImageView)findViewById( R.id.ivPedidosAdmin );
        ivMiGanancia= (ImageView)findViewById( R.id.ivMiGanancia );
        ivSalir = (ImageView)findViewById( R.id.ivSalir );

        ivMisProductos.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent( InicioAdmin.this, MisProductos.class );
                startActivity( intent );
            }
        } );

        ivSubirProducto.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent( InicioAdmin.this, SubirProducto.class );
                startActivity( intent );
            }
        } );

        ivPedidoAdmin.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent( InicioAdmin.this, PedidosAdmin.class );
                startActivity( intent );
            }
        } );

        ivMiGanancia.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent( InicioAdmin.this, Ganancia.class );
                startActivity( intent );
            }
        } );

        ivMisProductos.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent( InicioAdmin.this, MisProductos.class );
                startActivity( intent );
            }
        } );

        ivSalir.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent( InicioAdmin.this, Login.class );
                startActivity( intent );
            }
        } );

    }
}
