package semanaactual.maykelalvarado.facci.proyecto;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class Login extends AppCompatActivity {

    public ImageView ivAdmin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_login );

        ivAdmin = (ImageView)findViewById( R.id.ivAdmin );

        ivAdmin.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent( Login.this, LoginAdmin.class );
                startActivity( intent );
            }
        } );
    }
}
