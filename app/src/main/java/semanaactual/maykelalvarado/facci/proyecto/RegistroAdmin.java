package semanaactual.maykelalvarado.facci.proyecto;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class RegistroAdmin extends AppCompatActivity {

    Button btnEnviarAdmin, btnRegresarAdmin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_registro_admin );

        btnEnviarAdmin = (Button)findViewById( R.id.btnEnviarAdmin );
        btnRegresarAdmin = (Button)findViewById( R.id.btnRegresarAdmin );

        btnEnviarAdmin.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText( getApplicationContext(), "REGISTRO EXITOSO", Toast.LENGTH_LONG ).show();
            }
        } );


        btnRegresarAdmin.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent( RegistroAdmin.this, LoginAdmin.class );
                startActivity( intent );
            }
        } );
    }
}
